import manager.SocketManager;

import java.net.InetSocketAddress;

public class Main {
    public static void main(String[] arg){
        String host;

        int port;
        if(arg.length == 2) {
            host = arg[0];
            try {
                port = Integer.parseInt(arg[1]);
            } catch(Exception e) {
                e.printStackTrace();
                port = 1337;
            }
        }else {
            host = "localhost";
            port = 1337;
        }

        SocketManager socketManager = new SocketManager(new InetSocketAddress(host, port));
        System.out.println("Starting up...");
        socketManager.start();
    }
}
