package entity;

import org.java_websocket.WebSocket;

import java.util.UUID;

public class Client {
    public Client(UUID conn, WebSocket webSocket){
        this.selfClientId = conn;
        this.webSocket = webSocket;
    }

    //This is not used now
    public final UUID getSelfClientId() {
        return selfClientId;
    }

    public final Room getSelfRoom() {
        return selfRoom;
    }

    public void setRoom(Room room){
        selfRoom = room;
    }

    public final WebSocket getWebSocket() {
        return webSocket;
    }

    private final UUID selfClientId;

    private final WebSocket webSocket;

    private Room selfRoom;
}
