package manager;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import entity.Client;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.net.InetSocketAddress;
import java.util.*;

public class SocketManager extends WebSocketServer {

    RoomManager roomManager;
    Map<UUID, Client> clientList;

    public SocketManager(InetSocketAddress address){
        super(address);
        roomManager = new RoomManager();
        clientList = Collections.synchronizedMap(new HashMap<>());
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote){
        removeClient(conn.getAttachment(), "disconnect");
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        JsonObject jsonObject = JsonParser.parseString(message).getAsJsonObject();
        eventHandler(conn, jsonObject);
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        if(conn != null)
            if(conn.isClosing())
                removeClient(conn.getAttachment(), ex.getMessage());
    }

    @Override
    public void onStart() {
        System.out.println("Server started up!");
    }



    private void eventHandler(WebSocket conn, JsonObject jsonObject){
        Client client = clientList.get((UUID)conn.getAttachment());
        switch (jsonObject.get("event").getAsString()) {
            case "registerClient" -> createClient(conn, jsonObject);
            case "addVideo" -> {
                String url = jsonObject.get("videoUrl").getAsString();
                client.getSelfRoom().addVideo(url);
            }
            case "playVideo" -> client.getSelfRoom().broadcastVideoToggle(client, "play");
            case "stopVideo" -> client.getSelfRoom().broadcastVideoToggle(client, "stop");
            case "removeVideo" -> client.getSelfRoom().removeVideo(jsonObject.get("videoUrl").getAsString());
            case "skipToTime" -> client.getSelfRoom().broadcastVideoToggle(client, "time", jsonObject.get("time").getAsString());
            case "syncPush" -> client.getSelfRoom().syncWaitingClients(jsonObject.get("time").getAsString());
            default -> System.out.println(jsonObject);
        }
    }

    private void createClient(WebSocket conn, JsonObject jsonObject){
        System.out.println("Registering Client: " + jsonObject);
        if(conn.getAttachment()!=null)
            return;
        conn.setAttachment(UUID.randomUUID());
        Client newClient = new Client(conn.getAttachment(),conn);
        synchronized (clientList) {
            clientList.put(conn.getAttachment(), newClient);
        }
        roomManager.joinClientOnRoom(newClient, jsonObject.get("room").getAsString());
    }

    //need to catch an error because a client that loses connection fires the error event more than once
    private void removeClient(UUID uuid, String reason){
        System.out.println("removing client, reason: " + reason);
        try {
            Client client = clientList.get(uuid);
            roomManager.deregisterClient(client);
            synchronized (clientList) {
                clientList.remove(uuid);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public final RoomManager getRoomManager() {
        return roomManager;
    }
}
