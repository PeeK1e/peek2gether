package util;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LinkSanitizerTest {
    String[] listInput = {
        "http://www.youtube.com/watch?v=dQw4w9WgXcQ&a=GxdCwVVULXctT2lYDEPllDR0LRTutYfW",
        "http://www.youtube.com/watch?v=dQw4w9WgXcQ",
        "http://youtu.be/dQw4w9WgXcQ",
        "http://www.youtube.com/embed/dQw4w9WgXcQ",
        "http://www.youtube.com/v/dQw4w9WgXcQ",
        "http://www.youtube.com/e/dQw4w9WgXcQ",
        "http://www.youtube.com/watch?v=dQw4w9WgXcQ",
        "http://www.youtube.com/watch?feature=player_embedded&v=dQw4w9WgXcQ",
        "http://www.youtube-nocookie.com/v/6L3ZvIMwZFM?version=3&hl=en_US&rel=0"
    };
    String[] expectedResult = {
            "dQw4w9WgXcQ",
            "dQw4w9WgXcQ",
            "dQw4w9WgXcQ",
            "dQw4w9WgXcQ",
            "dQw4w9WgXcQ",
            "dQw4w9WgXcQ",
            "dQw4w9WgXcQ",
            "dQw4w9WgXcQ",
            "6L3ZvIMwZFM"
    };

    @Test
    public void testLinkSanitizer(){
        String[] generatedList = new String[listInput.length];

        for(int i = 0; i < listInput.length; i++){
            generatedList[i] = LinkSanitizer.cleanLink(listInput[i]);
        }

        assertTrue(Arrays.equals(generatedList, expectedResult));
    }
}
