package manager;
import entity.Client;
import entity.Room;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class RoomManager {
    public RoomManager(){
        roomList = Collections.synchronizedMap(new HashMap<>());
    }

    public void joinClientOnRoom(Client client, String room){
        Room roomToJoin = roomList.get(room);
        if(roomToJoin == null)
            roomToJoin = createAndAddRoom(room);
        roomToJoin.addClient(client);
        System.out.println("Client: " + client + "Registered on Room: " + client.getSelfRoom().getRoomName());
        System.out.println("Rooms: " + roomList.size());
    }

    public void deregisterClient(Client client){
        System.out.println("Removing Client: " + client);
        //This needs to be done better!
        String rName = client.getSelfRoom().getRoomName();
        roomList.get(rName).removeClient(client);
        if(roomList.get(rName).isRoomEmpty())
            roomList.remove(rName);
        System.out.println(roomList);
    }

    private Room createAndAddRoom(String roomName){
        Room room = new Room(roomName);
        synchronized (roomList) {
            roomList.put(roomName, room);
        }
        return room;
    }

    public final Map<String, Room> getRoomList() {
        return roomList;
    }

    private Map<String, Room> roomList;
}
