package entity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import util.LinkSanitizer;

import java.util.List;
import java.util.Queue;
import java.util.ArrayList;
import java.util.ArrayDeque;

public class Room {
    public Room(String roomName) {
        this.roomName = roomName;
        clientList = new ArrayList<>();
        videoList = new ArrayList<>();
        waitingForSync = new ArrayDeque<>();
    }

    public void addClient(Client client){
        client.setRoom(this);
        addClientToSyncCycle(client);
        synchronized (clientList){
            clientList.add(client);
        }
    }

    public void removeClient(Client client){
        synchronized (clientList) {
            clientList.remove(client);
        }
    }

    public void broadcastVideoToggle(Client client, String string){
        broadcastVideoToggle(client, string, null);
    }

    //may use String... time to use as optional parameter
    public void broadcastVideoToggle(Client client, String string, String time){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("event","videoToggle");
        jsonObject.addProperty("type", string);
        if(string.equals("time"))
            jsonObject.addProperty("time", time);
        for (Client value : clientList) {
            if (!value.equals(client))
                value.getWebSocket().send(jsonObject.toString());
        }
    }

    public String getNextVideo() {
        return videoList.stream().findFirst().orElse("");
    }

    //May need to use mutex to secure read write access
    public void addVideo(String video){
        video = LinkSanitizer.cleanLink(video);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("event","addVideo");
        jsonObject.addProperty("videoId",video);
        for(int i = 0; i < clientList.size(); i++){
            System.out.println("Send to entity.Client #" + i + " Message: " + jsonObject);
            clientList.get(i).getWebSocket().send(jsonObject.toString());
        }
        synchronized (videoList){
            videoList.add(video);
        }
    }

    public void addClientToSyncCycle(Client client){
        if(clientList.size() == 0)
            return;
        synchronized (waitingForSync){
            waitingForSync.add(client);
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("event", "syncRequest");
        //entity.Client 0 is usually Host
        clientList.get(0).getWebSocket().send(jsonObject.toString());
    }

    public void syncWaitingClients(String time){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("event", "syncUp");
        if(videoList.size()!=0) {
            jsonObject.addProperty("currentVideo", videoList.get(0));
            jsonObject.addProperty("queuedVideos", new Gson().toJson(videoList));
        }
        jsonObject.addProperty("currentTime", time);
        int queueSize = waitingForSync.size();
        String json = jsonObject.toString();
        for (int i = 0; i < queueSize; i++) {
            synchronized (waitingForSync){
                try{
                    waitingForSync.poll().getWebSocket().send(json);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    public void removeVideo(String video){
        synchronized (videoList){
            videoList.remove(video);
        }
    }

    public final String getRoomName() {
        return roomName;
    }

    public boolean isRoomEmpty(){
        return clientList.isEmpty();
    }

    public final List<Client> getClientList() {
        return clientList;
    }

    private List<String> videoList;

    private List<Client> clientList;

    private Queue<Client> waitingForSync;

    private String roomName;

}
