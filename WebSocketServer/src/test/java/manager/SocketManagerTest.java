package manager;
import com.google.gson.JsonObject;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.junit.jupiter.api.*;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

public class SocketManagerTest{
    private static List<WebSocketClient> clientList;
    private static SocketManager socketManager;
    private static final int clientCount = 15;
    private static final int roomSplit = 4;
    private static int port = 0;

    @BeforeAll
    static void setUp() {
        port = ThreadLocalRandom.current().nextInt(50000, 60000 + 1);
        socketManager = new SocketManager(new InetSocketAddress("localhost", port));
        socketManager.start();
        clientList = new ArrayList<>();
        for(int i = 0; i < clientCount; i++){
            try {
                String str = String.valueOf(i%4);
                WebSocketClient cli = new WebSocketClient(new URI("ws://localhost:" + port)) {
                    @Override
                    public void onOpen(ServerHandshake handshakedata) {
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("event", "registerClient");
                        jsonObject.addProperty("room", str);
                        this.send(jsonObject.toString());
                    }

                    @Override
                    public void onMessage(String message) {

                    }

                    @Override
                    public void onClose(int code, String reason, boolean remote) {

                    }

                    @Override

                    public void onError(Exception ex) {
                    }
                };

                clientList.add(cli);
                cli.connect();
                while(!cli.isOpen());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
}

    @AfterAll
    static void tearDown(){
        try {
            socketManager.stop();
            clientList.clear();
        }catch (InterruptedException | IOException e){
            e.printStackTrace();
        }
    }

    @Test
    void testRoomsCreated() {
        AtomicInteger i = new AtomicInteger();
        socketManager.getRoomManager().getRoomList().forEach((s, room) -> {
            i.getAndIncrement();
        });
        assertSame(roomSplit, i.get());
    }

    @Test
    void destroyRoomTest() throws InterruptedException {
        for (WebSocketClient client: clientList) {
            client.close();
            while(!client.isClosed());
        }
        //Race Condition, Clients are still being processed when assertSame() is called
        Thread.sleep(1000);
        assertSame(0, socketManager.roomManager.getRoomList().size());
    }
}
