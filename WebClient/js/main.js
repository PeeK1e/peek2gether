let webSocket; //change to real URL later
let player;

window.onload = () => {
  let name = new URLSearchParams(window.location.search).get("room");
  if(name != null) {
    startSocket(name);
    return;
  }
  document.getElementById("setRoomNameButton")
    .addEventListener("click", getOrGenerateRoomName);
  document.getElementById("roomNameField").
      addEventListener("keydown", textInputHandler);
  document.getElementById("content").style.visibility = "visible";
}

function textInputHandler(ev){
  if(13 === ev.keyCode) {
    ev.preventDefault();
    getOrGenerateRoomName();
  }
}

function startSocket(roomAddress){
  webSocket = new WS(roomAddress, player);
  loadVideoPlayer();
}

function getOrGenerateRoomName() {
  let field = document.getElementById("roomNameField");
  let url = new URLSearchParams(window.location.search);
  if (!(field.value === "")) {
    url.append("room", field.value);
    window.location.search = url;
  } else {
    let name = Math.random().toString(16).substr(2, 12);
    url.append("room", name);
    window.location.search = url;
    window.location.searchParams.append("room", name);
  }
}

function loadVideoPlayer(){
  let req = new XMLHttpRequest();
  req.open("GET", "./videoPlayer.html", true);
  req.onreadystatechange = () => {
    if(req.readyState === 4 && req.status === 200) {
      document.getElementById("content").innerHTML = req.responseText;
      console.log(req.responseText);
      player = new YT.Player('player',
        {
          videoId: null
        }
      );
      document.getElementById("content").style.visibility = "visible";
    }
  }
  req.send(null);
}

class WS {
  constructor(roomAddress) {
    this.webSocket = new WebSocket("ws://" + window.location.hostname + ":1337");
    this.webSocket.onopen = (ev) => {
      this.#registerClient(roomAddress);
    }

    this.webSocket.onerror = (ev) => {
      //setTimeout(()=>{this.webSocket = new WebSocket("ws://" +  document.location.host +":1337");},4000);
      alert("Session error");
    }

    this.webSocket.onmessage = (ev) => {
      this.#eventHandler(ev.data);
    }
  }

  #eventHandler(ev){
    let json = JSON.parse(ev);
    let response = {};
    switch (json.event) {
      case "syncRequest":
        response.event = "syncPush";
        try {
          response.time = player.getCurrentTime();
        }catch (e){
          console.log(e.toString());
          response.time = "0";
        }
        this.webSocket.send(JSON.stringify(response));
        break;
      default:
        console.log(ev);
        console.log(json);
        console.log(json.event);
    }
  }
  #registerClient(room = ""){
    let obj = {};
    obj.event = "registerClient";
    obj.room = room;
    let message = JSON.stringify(obj);
    console.log(message);
    this.webSocket.send(message);
  }

  addVideo(url= "https://youtu.be/fLlwCEZoG8Y?list=RDMMfLlwCEZoG8Y"){
    let obj = {};
    obj.event = "addVideo";
    obj.videoUrl = url;
    this.webSocket.send(JSON.stringify(obj));
  }


}


