package util;

import java.net.URI;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkSanitizer {
    private LinkSanitizer() {}

    public static String cleanLink(String url){
        if(!(url.contains("youtu.be") || url.contains("youtube")))
            return null;

        //String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);

        if(matcher.find()){
            return matcher.group();
        }
        return null;
    }
}
